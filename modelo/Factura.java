package modelo;

import javax.swing.*;
import java.util.*;
import java.text.*;

import utileria.Util;

// En construccion...
/**
 * Clase que representa una Factura.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class Factura {

    private int numero         = -1;
    private Cliente cliente    = null;
    private Date fecha         = null;
    private double valorIva    = 0.0;
    private double montoIva    = 0.0;
    private double subTotal    = 0.0;
    private double total       = 0.0;

    private final int LIMITEDETALLES = 10;

    // ArrayList que contiene los detalles de la factura
    private ArrayList<DetalleFactura> listaDetalles = null;
  
    /**
     * Constructor
     * @param listaD ArrayList<DetalleFactura> referencia a la lista detalles.
     */
    public Factura(ArrayList<DetalleFactura> listaD) {
        this(listaD, -1, null, null, 0.0, 0.0);
    }
    
    public Factura(ArrayList<DetalleFactura> listaD, int num, Cliente cli, Date fecha, double iva, double total) {
        this.listaDetalles = listaD;
        setNumero(num);
        setCliente(cli);
        setFecha(fecha);
        setValorIva(iva);
        setTotal(total);
    }
    
    // Setters
    public void setNumero(int numero) {
        this.numero = numero;
    }
    public void setCliente(Cliente cli) {
        if (cli == null)
            return;
        this.cliente = cli;
    }
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public boolean setValorIva(double valorIva) {
        if (valorIva < 0.0)
            return false;
        this.valorIva = valorIva;
        actualizarValores();
        return true;
    }
    private boolean setMontoIva(double montoIva) {
        if (montoIva < 0.0)
            return false;
        this.montoIva = montoIva;
        return true;
    }
    private boolean setSubTotal(double subTotal) {
        if (subTotal < 0.0)
            return false;
        this.subTotal = subTotal;
        return true;
    }
    private boolean setTotal(double total) {
        if (total < 0.0)
            return false;
        this.total = total;
        return true;
    }
   
    // Getters
    public int getNumero() {
        return this.numero;
    }
    public Cliente getCliente() {
        return this.cliente;
    }
    public Date getFecha() {
        return this.fecha;
    }
    public double getValorIva() {
        return this.valorIva;
    }
    public double getMontoIva() {
        return this.montoIva;
    }
    public double getSubTotal() {
        return this.subTotal;
    }
    public double getTotal() {
        return this.total;
    }
    
    // Customs
    
    /**
     * Metodo que recorre todos los detalles de la factura
     * y calcular el subtotal.
     */
    private void calcularSubTotal() {
        Double acc = 0.0;
        for (DetalleFactura df : listaDetalles)
            acc += df.getImporte();
        acc = Util.redondeo(acc, 2);
        setSubTotal(acc);
    }
    
    /**
     * Metodo que apartir del subtotal calcular el montoIva
     * segun sea el porcentaje de IVA.
     */
    private void calcularMontoIva() {
        Double monto = new Double(getSubTotal() * (getValorIva() / 100));
        monto = Util.redondeo(monto, 2);
        setMontoIva(monto);
    }
    
    /**
     * Metodo que apartir del subtotal y el monto de iva calcula
     * el monto total de la factura.
     */
    private void calcularTotal() {
        Double total = getSubTotal() + getMontoIva();
        total = Util.redondeo(total, 2);
        setTotal(total);
    }
    
    /**
     * Metodo que actualiza el subtotal, total iva y total general.
     */
    private void actualizarValores() {
        if (listaDetalles == null)
            return;
        calcularSubTotal();
        calcularMontoIva();
        calcularTotal();
    }
    
    /**
     * Metodo que apartir de un DetalleFactura recibido por parametro lo agrega
     * a la lista si este no se encuentra previamente cargado.
     * @param nuevoDetalle es un DetalleFactura a ser agregado.
     * @return boolean true si se agrego correctamente, false en caso contrario.
     */
    public boolean agregarDetalle(DetalleFactura nuevoDetalle) {
        if (getListaDetalles().size() == LIMITEDETALLES)
            return false;
        if (getListaDetalles().contains(nuevoDetalle)) {
            return false;
        }
        getListaDetalles().add(nuevoDetalle);
        actualizarValores();
        return true;
    }

    /**
     * Metodo que elimina un DetalleFactura de la lista tomando como referencia
     * el indice del Detalle dentro de ListaDetalles.
     * @param indice (de ListaDetalles) indica el Detalle a eliminar.
     * @return boolean true si se elimino correctamente, false en caso contrario.
     */
    public boolean eliminarDetalle(int indice) {
        if (indice < 0 || indice > getListaDetalles().size())
            return false;
        getListaDetalles().remove(indice);
        actualizarValores();
        return true; 
    }

    /**
     * Metodo que modifica un DetalleFactura de la lista tomando como referencia
     * el indice del Detalle dentro de ListaDetalles y el nuevoDetalle modificado.
     * @param indice (de ListaDetalles) indica el Detalle a modificar.
     * @param nuevoDetalle es el nuevo DetalleFactura modificado.
     * @return boolean true si se modifico correctamente, false en caso contrario.
     */
    public boolean modificarDetalle(DetalleFactura nuevoDetalle, int indice) {
        if (indice < 0 || indice > getListaDetalles().size())
            return false;
        if (getListaDetalles().get(indice).getId() != nuevoDetalle.getId() && getListaDetalles().contains(nuevoDetalle) )
            return false;
        getListaDetalles().set(indice, nuevoDetalle);
        actualizarValores();
        return true;
    }
    
    /**
     * Metodo que limpia la lista de detalles.
     */
    public void limpiarLista() {
        getListaDetalles().clear();
        actualizarValores();
    }

    /**
     * Metodo que verifica que se pueda facturar.
     * @return true si es posible, false caso contrario.
     */
    public boolean puedeFacturar() { //|| getCuitCliente() == null 
        if ( getNumero() == -1 || getFecha() == null || getCliente() == null)
            return false;
        if (getListaDetalles().size() == 0)
            return false;
        return true;
    }
    
    /**
     * Metodo que recorre la lista de detalles y las agrega en un StringBuffer
     * para luego retornar la representacion en formato String.
     * @return String la lista de detalles completa.
     */
    private String mostrarDetalles() {
        StringBuffer buffer = new StringBuffer();
        for (DetalleFactura df : listaDetalles) {
            buffer.append(df + "\n");
        }
        return buffer.toString();
    }
    
    /**
     * Metodo que retorna el ArrayList con todos los detalles.
     * @return listaDetalles ArrayList<DetalleFactura>.
     */
    public ArrayList<DetalleFactura> getListaDetalles() {
        return listaDetalles;
    }

    @Override
    public String toString() {
        return "Numero Factura : " + getNumero()      + "\n" +
               "Cliente : [\n"        + getCliente()     + "]\n" +
               "Fecha : "          + getFecha()       + "\n" +
               "Valor Iva : "      + getValorIva()    + "\n" +
               "Monto Iva : "      + getMontoIva()    + "\n" +
               "Subtotal : "       + getSubTotal()    + "\n" +
               "Total : "          + getTotal()       + "\n" +
               "Detalles : \n"     + mostrarDetalles();
    }
}
