package modelo;

import java.io.*;
 
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
 
import java.util.*;

import utileria.Util;

/**
 * Clase que setea una pdf parametrizado (a partir de modelo Factura) el cual representa
 * visualmente una factura. Se utiliza la libreria itextpdf.
 * @author ISFT-189
 * @version 1.0
 */
public class GenerarPDF {
 
    public static final String RUTAMODELOFACTURA = "./resources/modeloFactura.pdf";
    public static final String RUTADESTINO       = "./resources/factura.pdf";

    private Factura fact = null;

    /**
     * Constructor
     * @param miFact referencia al modelo Factura.
     */
    public GenerarPDF(Factura miFact) {
        this.fact = miFact;
    }

    /**
     * Metodo encargado de setear los campos de texto del pdf
     * con los valores del modelo Factura.
     */
    public boolean realizarPDF() {
        boolean ret = false;
        if (this.fact == null)
            return false;
        try {
            PdfReader reader   = new PdfReader(RUTAMODELOFACTURA);
            PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RUTADESTINO));
 
            AcroFields form = stamper.getAcroFields();

            StringBuffer bufferCantidad    = new StringBuffer();
            StringBuffer bufferDescripcion = new StringBuffer();
            StringBuffer bufferPrecio      = new StringBuffer();
            StringBuffer bufferImporte     = new StringBuffer();
            
            form.setField("txtNumero",    String.valueOf(fact.getNumero()) );
            form.setField("txtTipo",      "A"); // ..
            form.setField("txtFecha",     fact.getFecha().toString());
            form.setField("txtCliente",   fact.getCliente().getApellido() + " " + fact.getCliente().getNombre());
            form.setField("txtDomicilio", fact.getCliente().getDireccion());
            form.setField("txtCUIT",      fact.getCliente().getCuit());
            
            for (DetalleFactura df : fact.getListaDetalles()) {
               bufferCantidad.append(    Util.seteoCampoDetalle( String.valueOf(df.getCantidad()) , 35) + "\n");
               bufferDescripcion.append( Util.seteoCampoDetalle( df.getDescripcion().trim(), 70) + "\n");
               bufferPrecio.append(      Util.seteoCampoDetalle( String.valueOf(df.getPrecioUnitario()) , 30) + "\n" );
               bufferImporte.append(     Util.seteoCampoDetalle( String.valueOf(df.getImporte()) , 10) + "\n");
            }

            form.setField("txtColumnaCantidad",    bufferCantidad.toString() );
            form.setField("txtColumnaDescripcion", bufferDescripcion.toString() );
            form.setField("txtColumnaPrecio",      bufferPrecio.toString() );
            form.setField("txtColumnaImporte",     bufferImporte.toString() );
            form.setField("txtSubtotal",           String.valueOf(fact.getSubTotal()) );
            form.setField("txtIVA",                String.valueOf(fact.getMontoIva()) );
            form.setField("txtTotal",              String.valueOf(fact.getTotal()) );

            stamper.close();
            reader.close();
            ret = true;
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
            ret = false;
        } catch (DocumentException de) {
           System.err.println(de.getMessage());
           ret = false;
        } finally {
            return true;
        }
    }
}