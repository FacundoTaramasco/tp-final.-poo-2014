
import javax.swing.UIManager;

import vista.GUIMain;

/**
* Clase Main del sistema la cual llama a la interfaz
* principal
* @author ISFT-189
* @version 1.0
*/
public class Main {

    /*************************************** MAIN ***************************************/
    public static void main(String[] args) {
        // jugando con la apariencia de swing
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        new GUIMain();
        String prueba = new String();
       
    }
    /************************************************************************************/
}
