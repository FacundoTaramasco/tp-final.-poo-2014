package modelo;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import java.sql.*;

import utileria.Util;

/**
 * Clase que hereda a AbstractTableModel y es utilizada para representar un
 * modelo de tabla especifica de Productos.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class ModeloTablaProductos extends AbstractTableModel {

    private Connection cnx        = null;
    private ResultSet rs          = null;

    private ArrayList<Producto> listaProductos = new ArrayList<Producto>();

    private final String[] queryUpdate = {"UPDATE Producto set id = ? WHERE id = ! ",
                                          "UPDATE Producto set descripcion = '?' WHERE id = ! ",
                                          "UPDATE Producto set cantidad = ? WHERE id = !",
                                          "UPDATE Producto set precioUnitario = ? WHERE id = !"};
    /**
     * Constructor
     * @param cnx referencia de tipo Connection a la conexion de la bbdd
     */
    public ModeloTablaProductos(Connection cnx) {
        this(cnx, "SELECT id, descripcion, cantidad, precioUnitario FROM Producto");
    }

    /**
     * Constructor
     * @param cnx referencia de tipo Connection a la conexion de la bbdd
     */
    public ModeloTablaProductos(Connection cnx, String queryProducto) {
        /*for(int x = 0 ; x < 10 ; x++) {
            getListaProductos().add( new Producto(x,"descripcion"+x,x,x) );
        }*/
        this.cnx = cnx;
        try {
            rs  = SimpleDataIO.getResultSet(cnx, queryProducto);
            while (rs.next()) {
                getListaProductos().add( new Producto(rs.getInt("id"),
                                                      rs.getString("descripcion").trim(),
                                                      rs.getInt("cantidad"),
                                                      rs.getDouble("precioUnitario")) );
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
        }
    }
    
    
    // --------------------------------------------- Metodos obligatorios --------------------------------------------------

    @Override
    public Object getValueAt(int fila, int columna) {
        switch(columna) {
            case 0 : return getListaProductos().get(fila).getId();
            case 1 : return getListaProductos().get(fila).getDescripcion();
            case 2 : return getListaProductos().get(fila).getCantidad();
            case 3 : return getListaProductos().get(fila).getPrecioUnitario();
        }
        return null;
    }
    
    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return listaProductos.size();
    }

    // ---------------------------------------------- Metodos adicionales -----------------------------------------------------
    
    @Override
    public boolean isCellEditable(int fila, int columna) {
        return true;
    }
    
    @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "ID";
            case 1 : return "Descripcion";
            case 2 : return "Cantidad";
            case 3 : return "Precio Unitario";
        }
        return null;
    }
    
    @Override
    public Class getColumnClass(int columna) {
        switch(columna) {
            case 0 : return Integer.class; // id
            case 1 : return String.class;  // descripcion
            case 2 : return Integer.class; // cantidad
            case 3 : return Double.class;  // precio unitario
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int fila, int columna) {
        /*
        para actualizar las celdas :
           -verificar que el dato sea valido (en el caso de la columna id tmb verificar que no se repita)
             -dato ok, actualizar dato
        */ 
        String query;
        Integer idAnterior   = getListaProductos().get(fila).getId();
        Producto modProducto = new Producto(getListaProductos().get(fila).getId(),
                                            getListaProductos().get(fila).getDescripcion(),
                                            getListaProductos().get(fila).getCantidad(),
                                            getListaProductos().get(fila).getPrecioUnitario());
        switch(columna) {
            case 0:
                if (!modProducto.setId((Integer)aValue)) {
                    Util.muestroError("ID Incorrecta"); return;
                }
                // id ok, chequeo que no se encuentre en la bbdd
                String queryAux = "SELECT id, descripcion, cantidad, precioUnitario " +
                "FROM Producto WHERE id = " + modProducto.getId() + " AND id <> " + idAnterior;
                ResultSet data = SimpleDataIO.getResultSet(this.cnx, queryAux);
                try {
                    if (data.next()) {
                        Producto productoExistente = new Producto(data.getInt("id"),
                                                                 data.getString("descripcion").trim(),
                                                                 data.getInt("cantidad"),
                                                                 data.getDouble("precioUnitario")); 
                        Util.muestroError("Producto Existente : \n " + productoExistente);
                        return;
                    }
                } catch (SQLException sqle) {
                    SimpleDataIO.showSQLException(sqle);
                    return;
                }
                break;
            case 1:
                if (!modProducto.setDescripcion((String)aValue)) {
                    Util.muestroError("Descripcion Incorrecta"); return;
                } 
                break;
            case 2:
                if (!modProducto.setCantidad((Integer)aValue)) {
                    Util.muestroError("Cantidad Incorrecta"); return;
                }
                break;
            case 3:
                if (!modProducto.setPrecioUnitario((Double)aValue)) {
                     Util.muestroError("Precio Incorrecto"); return;
                }
        }
        // seteo el query del update
        query = queryUpdate[columna].replace("?", aValue.toString()).replace("!", idAnterior.toString());        
        if (SimpleDataIO.executeQuery(cnx, query)) {
            SimpleDataIO.doCommit(cnx);
            getListaProductos().set(fila, modProducto);
            this.fireTableDataChanged();
            //Util.muestroMensaje("Producto modificado");
        }
    }
    
    /**
     * Metodo que retorna el ArrayList con todos los Productos.
     * @return listaProductos ArrayList<Producto>
     */
    public ArrayList<Producto> getListaProductos() {
        return listaProductos;
    }
    
    /**
     * Metodo que recibe un Producto y lo da de alta en la BBDD.
     * @param nuevoProd es el nuevo Producto a dar de altas.
     * @return boolean true si se agrego correctamente, false en caso contrario.
     */
    public boolean agregarProducto(Producto nuevoProd) {
        // armo query de consulta para chequear si ya existe un producto con la id ingresada
        String queryAux = "SELECT id, descripcion, cantidad, precioUnitario " +
        "FROM Producto WHERE id = " + nuevoProd.getId();

        ResultSet data = SimpleDataIO.getResultSet(this.cnx, queryAux);
        try {
            if (data.next()) { 
                Producto productoExistente = new Producto(data.getInt("id"),
                                                         data.getString("descripcion").trim(),
                                                         data.getInt("cantidad"),
                                                         data.getDouble("precioUnitario")); 
                Util.muestroError("Producto Existente : \n " + productoExistente);
                return false;
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
            return false;
        }
        // ID disponible, armo query de insert
        String query = "INSERT INTO Producto (id, descripcion, cantidad, precioUnitario) values (" +
                                           nuevoProd.getId() + ", '" + 
                                           nuevoProd.getDescripcion()  + "', " +
                                           nuevoProd.getCantidad()  + ", " +
                                           nuevoProd.getPrecioUnitario()  + ")";
        if (SimpleDataIO.executeQuery(this.cnx, query)) {
            SimpleDataIO.doCommit(this.cnx);
            getListaProductos().add(nuevoProd);
            this.fireTableDataChanged();
            return true;
        } else return false;
    }

    /**
     * Metodo que elimina un Producto del sistema tomando como referencia
     * el indice del producto dentro de ListaProductos.
     * @param indice (de listaProductos) indica el Producto a eliminar.
     * @return boolean true si se elimino correctamente, false en caso contrario.
     */
    public boolean eliminarProducto(int indice) {
        if (indice < 0 || indice > getListaProductos().size())
            return false;
        Producto productoEliminar = getListaProductos().get(indice);
        String query = "DELETE FROM Producto WHERE id = " + productoEliminar.getId();
        if (SimpleDataIO.executeQuery(cnx, query)) {
            SimpleDataIO.doCommit(cnx);
            getListaProductos().remove(indice);
            this.fireTableDataChanged();
            return true;
        } else return false;
    }
    
    public void udpdateProductos() {
        this.fireTableDataChanged();
    }
}