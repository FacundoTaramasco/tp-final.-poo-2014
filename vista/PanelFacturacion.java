package vista;

import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import modelo.Cliente;
import modelo.Factura;
import modelo.Producto;
import modelo.DetalleFactura;
import modelo.ModeloTablaDetalles;
import modelo.SimpleDataIO;

import utileria.Util;

/**
 * Clase que hereda de JInternalFrame usada para mostrar y gestionar los Facturas de la aplicacion.
 * @author ISFT-189
 * @version 1.0
 */
public class PanelFacturacion extends JInternalFrame implements ActionListener {

    private JPanel panelSuperior;
    private JPanel panelIzq;
    private JPanel panelDer;
    private JPanel panelCentral;
    private JPanel panelInferior;
    
    private JButton btnIniciarFactura;
    private JButton btnListadoFacturas;

    private JComboBox cbTipoFactura;
    private JTextField txtNroFactura;
    private JTextField txtFechaFactura;
    private JRadioButton rbContado;
    private JRadioButton rbCtaCte;

    private JTextField txtCUIT;
    private JTextField txtCliente;
    private JTextField txtDireccion;
    private JComboBox cbIVA;

    private JButton btnAgregaDetalle;
    private JButton btnEliminaDetalle;
    
    private JTable tablaDetalles;
    
    private JButton btnAceptar;
    private JButton btnCancelar;

    private JTextField txtSubTotal;
    private JTextField txtTotalIva;
    private JTextField txtTotal;

    //------------------------------
    private GridBagConstraints gbc = new GridBagConstraints();

    private Connection cnx;

    private PanelProductos panelProd;
    private PanelClientes  panelCli;
    
    private Factura miFactura;

    private ArrayList<DetalleFactura> listaDetalles = null;
    
    private ModeloTablaDetalles MTD = null;
    
    private HashMap<String, JComponent> mapaWidgets = new HashMap<String, JComponent>();
    //------------------------------
    
    /**
     * Constructor
     * @param con referencia de tipo Connection a la conexion de la bbdd
     * @param PanelProd referencia al panel de productos
     * @param PCli referencia al panel de clientes
     * Los paneles de clientes y productos son usados para habilitar y deshabilitar algunos
     * de sus widgets.
     */
    public PanelFacturacion(Connection con, PanelProductos PanelProd, PanelClientes PCli) {
        super("Facturacion", true, false, true, true);

        this.cnx       = con;
        this.panelProd = PanelProd;
        this.panelCli  = PCli;

        this.setLayout( new GridBagLayout() );
        
        /********************* PANEL SUPERIOR ********************************/
        
        panelSuperior = new JPanel( new GridBagLayout() );
        panelSuperior.setBorder(BorderFactory.createLineBorder(Color.black));

        btnIniciarFactura  = new JButton("Iniciar Factura");
        btnListadoFacturas = new JButton("Listado de Facturas");

        Util.agregarWidgetJC( panelSuperior, gbc, new JLabel("")    , 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelSuperior, gbc, btnIniciarFactura , 1, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelSuperior, gbc, btnListadoFacturas, 2, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelSuperior, gbc, new JLabel("")    , 3, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        
        /**********************************************************************/
        
        /********************* PANEL IZQUIERDO ********************************/
        panelIzq = new JPanel( new GridBagLayout() );
        //panelIzq.setBorder(BorderFactory.createLineBorder(Color.black));

        String[] tipoFac  = {"A", "B"};
        cbTipoFactura     = new JComboBox<String>(tipoFac);
        txtNroFactura     = new JTextField();
        txtFechaFactura   = new JTextField();

        txtNroFactura.setEditable(false);
        txtFechaFactura.setEditable(false);
        
        rbContado              = new JRadioButton("Contado", true);
        rbCtaCte               = new JRadioButton("Cuenta Corriente", false);
        ButtonGroup btnGrupo   = new ButtonGroup();
        btnGrupo.add(rbContado);
        btnGrupo.add(rbCtaCte);

        Util.agregarWidgetJC( panelIzq, gbc, new JLabel("Tipo Factura : ")         , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelIzq, gbc, cbTipoFactura                         , 1, 0, 3, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelIzq, gbc, new JLabel("Numero : ")               , 0, 1, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelIzq, gbc, txtNroFactura                         , 1, 1, 3, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelIzq, gbc, new JLabel("Fecha : ")                , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelIzq, gbc, txtFechaFactura                       , 1, 2, 3, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelIzq, gbc, new JLabel("Condiciones de venta : ") , 0, 3, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelIzq, gbc, rbContado                             , 1, 3, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelIzq, gbc, rbCtaCte                              , 2, 3, 1, 1, 0, 0, GridBagConstraints.NONE );

        /**********************************************************************/

        /********************* PANEL DERECHO ********************************/
        panelDer = new JPanel( new GridBagLayout() );
        //panelDer.setBorder(BorderFactory.createLineBorder(Color.black));

        txtCUIT      = new JTextField();
        txtCliente   = new JTextField();
        txtDireccion = new JTextField();
        txtCliente.setEditable(false);
        txtDireccion.setEditable(false);
        
        String[] tipoIVA = {"21.0", "10.5"};
        cbIVA            = new JComboBox<String>(tipoIVA);

        Util.agregarWidgetJC( panelDer, gbc, new JLabel("CUIT : ")      , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE ); 
        Util.agregarWidgetJC( panelDer, gbc, txtCUIT                    , 1, 0, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetJC( panelDer, gbc, new JLabel("Cliente : ")   , 0, 1, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelDer, gbc, txtCliente                 , 1, 1, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetJC( panelDer, gbc, new JLabel("Direccion : ") , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE ); 
        Util.agregarWidgetJC( panelDer, gbc, txtDireccion               , 1, 2, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetJC( panelDer, gbc, new JLabel("IVA : ")       , 0, 3, 1, 1, 0, 0, GridBagConstraints.NONE ); 
        Util.agregarWidgetJC( panelDer, gbc, cbIVA                      , 1, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelDer, gbc, new JLabel("")             , 2, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        /**********************************************************************/

        /********************* PANEL CENTRAL ********************************/
        panelCentral = new JPanel( new GridBagLayout() );
        panelCentral.setBorder(BorderFactory.createLineBorder(Color.black));

        btnAgregaDetalle    = new JButton("Agregar");
        btnEliminaDetalle   = new JButton("Eliminar");
        btnAceptar          = new JButton("Aceptar");
        btnCancelar         = new JButton("Cancelar");

        tablaDetalles       = new JTable(MTD);
        tablaDetalles.setToolTipText("Doble click en las celdas para modificar datos.");
        tablaDetalles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        Util.agregarWidgetJC( panelCentral, gbc, btnAgregaDetalle   , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelCentral, gbc, btnEliminaDetalle  , 1, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelCentral, gbc, new JLabel("")     , 2, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelCentral, gbc, btnAceptar         , 3, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelCentral, gbc, btnCancelar        , 4, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        
        Util.agregarWidgetJC( panelCentral, gbc, new JScrollPane(tablaDetalles)    , 0, 1, 5, 1, 1, 1, GridBagConstraints.BOTH );

        /**********************************************************************/

        /********************* PANEL INFERIOR ********************************/
        panelInferior = new JPanel( new GridBagLayout() );
        panelInferior.setBorder(BorderFactory.createLineBorder(Color.black));

        txtSubTotal = new JTextField();
        txtTotalIva = new JTextField();
        txtTotal    = new JTextField();
        
        txtSubTotal.setEditable(false);
        txtTotalIva.setEditable(false);
        txtTotal.setEditable(false);
        txtSubTotal.setText("0.0");
        txtTotalIva.setText("0.0");
        txtTotal.setText("0.0");

        Util.agregarWidgetJC( panelInferior, gbc, new JLabel("SubTotal : ")  , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelInferior, gbc, txtSubTotal                , 1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelInferior, gbc, new JLabel("Total IVA : ") , 2, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelInferior, gbc, txtTotalIva                , 3, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( panelInferior, gbc, new JLabel("Total : ")     , 4, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelInferior, gbc, txtTotal                   , 5, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        /**********************************************************************/
        
        // agregando paneles a jinternalframe
        Util.agregarWidgetJC( this, gbc, panelSuperior  , 0, 0, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( this, gbc, panelIzq       , 0, 1, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( this, gbc, panelDer       , 1, 1, 1, 1, 4, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( this, gbc, panelCentral   , 0, 2, 2, 1, 1, 1, GridBagConstraints.BOTH );
        Util.agregarWidgetJC( this, gbc, panelInferior  , 0, 3, 2, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        
        /**********************************************************************/

        /* *******************************************************************************************************
        ******************************************************************************************************* */
        
        // mapa de widgets utilizado en ModeloTablaDetalles para setear valores de jtextfield (subtotal, iva y total) de la factura
        mapaWidgets.put("txtSubTotal", txtSubTotal);
        mapaWidgets.put("txtTotalIva", txtTotalIva);
        mapaWidgets.put("txtTotal", txtTotal);
        
        // creando lista detalles usada tanto en la factura como en modelo tabla detalles
        listaDetalles = new ArrayList<DetalleFactura>();
        
        miFactura     = new Factura(listaDetalles);

        MTD           = new ModeloTablaDetalles(this.cnx, listaDetalles, miFactura, this);
        
        // estableciendo modelo a la tabla detalles
        tablaDetalles.setModel(MTD);
        
        /* *******************************************************************************************************
        ******************************************************************************************************* */

        this.estadoComponentes(false);

        // estableciendo eventos
        btnIniciarFactura.addActionListener(this);
        btnListadoFacturas.addActionListener(this);
        btnAceptar.addActionListener(this);
        btnCancelar.addActionListener(this);
        btnAgregaDetalle.addActionListener(this);
        btnEliminaDetalle.addActionListener(this);
        cbIVA.addActionListener(this);
        txtCUIT.addActionListener(this);
        
        // evento cambio de texto en txtCUIT
        txtCUIT.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                seteoCUIT();
            }
            public void removeUpdate(DocumentEvent e) {
                seteoCUIT();
            }
            public void insertUpdate(DocumentEvent e) {
                seteoCUIT();
            }
            public void seteoCUIT() {
                txtCliente.setText("");
                txtDireccion.setText("");
                miFactura.setCliente(null);
            }
        });
    }

    /**
     * Metodo que recibe un String representando una clave del mapa y retorna
     * el JComponent a cual esta eferencia.
     * @param widget String clave del mapa.
     * @return JComponent componente asociado a la clave.
     */
    public JComponent getWidgetMapa(String widget) {
        return this.mapaWidgets.get(widget);
    }
    
    /**
     * Metodo que setea texto a vacio a los JTextField
     */
    public void limpiarWidgets() {
        txtNroFactura.setText("");
        txtCUIT.setText("");
        txtCliente.setText("");
        txtDireccion.setText("");
        txtFechaFactura.setText("");
    }
    
    /**
     * Metodo que establece el estado de algunos Widgets.
     */
    public void estadoComponentes(boolean estado) {
        for(Component c : panelIzq.getComponents())
            c.setEnabled(estado);
        for(Component c : panelDer.getComponents())
            c.setEnabled(estado);
        for(Component c : panelCentral.getComponents())
            c.setEnabled(estado);
        tablaDetalles.setEnabled(estado);
        for(Component c : panelInferior.getComponents())
            c.setEnabled(estado);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnIniciarFactura) {   /** click en btnIniciarFactura **/
            ResultSet data           = null;
            java.util.Date diaActual = null;
            int numeroFac   = 0;
            double valorIva = 0.0;
            this.estadoComponentes(true);
            panelProd.estadoComponentes(false);
            panelCli.estadoComponentes(false);
            try {
                data = SimpleDataIO.getResultSet(this.cnx, "select current_date, generarnumerofactura()");
                data.next();
                diaActual   = data.getDate(1);
                numeroFac   = data.getInt(2);
                valorIva    = Double.parseDouble(cbIVA.getSelectedItem().toString());
                miFactura.setNumero(numeroFac);
                miFactura.setFecha(diaActual);
                miFactura.setValorIva(valorIva);
                txtFechaFactura.setText(diaActual.toString());
                txtNroFactura.setText(String.valueOf(numeroFac));
            } catch (SQLException sqle) {
                SimpleDataIO.showSQLException(sqle);
            }
        }
        if (e.getSource() == btnListadoFacturas) {
            new ListadoFacturas(cnx);
        }
        if (e.getSource() == btnAceptar) {        /** click en btnAceptar **/
            if ( miFactura.puedeFacturar() ) {
                try {
                    int indice   = -1;
                    int cantidad = 0;
                    CallableStatement callFunction = null;
                    // agrego factura
                    callFunction = cnx.prepareCall("{call agregarFactura(?, ?, ?, ?, ?)}");
                    callFunction.setInt(    1, miFactura.getNumero() );
                    callFunction.setString( 2, miFactura.getCliente().getCuit() );
                    callFunction.setDate(   3, (java.sql.Date)miFactura.getFecha() );
                    callFunction.setDouble( 4, miFactura.getValorIva() );
                    callFunction.setDouble( 5, miFactura.getTotal() );
                    callFunction.execute();
                    // agrego detalles
                    callFunction = cnx.prepareCall("{call agregardetalle(?, ?, ?, ?)}");
                    for (DetalleFactura dt : miFactura.getListaDetalles()) {
                        callFunction.setInt(    1, dt.getId() );
                        callFunction.setInt(    2, miFactura.getNumero() );
                        callFunction.setInt(    3, dt.getCantidad() );
                        callFunction.setDouble( 4, dt.getPrecioUnitario() );
                        callFunction.execute();
                        // tomo el indice del producto en 'ListaProductos' al cual se le descontara el stock
                        indice   = panelProd.getMTP().getListaProductos().indexOf(new Producto(dt.getId()));
                        // tomo la cantidad antigua
                        cantidad = panelProd.getMTP().getListaProductos().get(indice).getCantidad();
                        // seteo la nueva cantidad (resto el stock facturado)
                        panelProd.getMTP().getListaProductos().get(indice).setCantidad( cantidad - dt.getCantidad() );
                    }
                    SimpleDataIO.doCommit(cnx);
                    panelProd.getMTP().udpdateProductos(); // actualio jtable productos
                    this.MTD.limpiarLista(); // limpio los detalles
                    this.limpiarWidgets();   // seteo los widgets
                    this.estadoComponentes(false);
                    panelProd.estadoComponentes(true);
                    panelCli.estadoComponentes(true);
                    //Util.muestroMensaje("deberia generar factura...\n" + miFactura);
                    Util.muestroMensaje("Factura generada Correctamente.");
                } catch (SQLException sqle ) {
                    SimpleDataIO.showSQLException(sqle);
                }
            }
            else Util.muestroError("No puede facturar");
        }
        if (e.getSource() == btnCancelar) {       /** click en btnCancelar **/
            this.estadoComponentes(false);
            panelProd.estadoComponentes(true);
            panelCli.estadoComponentes(true);
            this.MTD.limpiarLista();
            this.limpiarWidgets();
        }
        if (e.getSource() == btnAgregaDetalle) {  /** click en btnAgregaDetalle **/
            new AgregarDetalle(this.cnx, MTD);
        }
        if (e.getSource() == btnEliminaDetalle) { /** click en btnEliminaDetalle **/
            int indice = tablaDetalles.getSelectedRow();
            if ( indice != -1) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog (null, "¿Desea Eliminar el Detalle?\n" +
                                                             miFactura.getListaDetalles().get(indice), "Eliminar Detalle", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    if (MTD.eliminarDetalle(indice))
                        Util.muestroMensaje("Detalle Eliminado");
                }
            } 
        }
        if (e.getSource() == cbIVA) {    /** item changed cbIVA **/
            miFactura.setValorIva(Double.parseDouble(cbIVA.getSelectedItem().toString()));
            this.MTD.actualizarValoresWidget();
        }
        if (e.getSource() == txtCUIT) {  /** enter en txtCUIT **/
            // al realizar enter en txtcuit se muestra los datos del cliente segun el cuit ingresado
            String cuit    = null;
            ResultSet data = null;
            Cliente cli    = null;
            txtCliente.setText("");
            txtDireccion.setText("");
            miFactura.setCliente(null);
            if (txtCUIT.getText().length() != 11)
                return;
            try {
                cuit = txtCUIT.getText();
                data = SimpleDataIO.getResultSet(this.cnx, "SELECT cuit, apellido, nombre, direccion, telefono " +
                                                                     "FROM Cliente where cuit = '" + cuit + "'");
                if (data.next()) {
                    cli = new Cliente(cuit,
                                      data.getString("apellido").trim(),
                                      data.getString("nombre").trim(),
                                      data.getString("direccion").trim(),
                                      data.getString("telefono").trim());
                    txtCliente.setText( cli.getApellido() + " " + cli.getNombre() );
                    txtDireccion.setText( cli.getDireccion() );
                    // seteo el cliente en la factura
                    miFactura.setCliente(cli);
                }
            } catch(SQLException sqle) {
                SimpleDataIO.showSQLException(sqle);
            }
        }
    }
}
