package modelo;

import utileria.Util;

/**
 * Clase simple POJO que representa un Cliente.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class Cliente {

    private String cuit;
    private String apellido;
    private String nombre;
    private String direccion;
    private String telefono;

    public Cliente() {
        this("00000000000");
    }
    public Cliente(String cuit) {
        this(cuit, "Apellido Default");
    }
    public Cliente(String cuit, String apellido) {
        this(cuit, apellido, "Nombre Default");
    }    
    public Cliente(String cuit, String apellido, String nombre) {
        this(cuit, apellido, nombre, "Direccion Default");
    }
    public Cliente(String cuit, String apellido, String nombre, String direccion) {
        this(cuit, apellido, nombre, direccion, "Telefono Default");
    }
    public Cliente(String cuit, String apellido, String nombre, String direccion, String telefono) {
        setCuit(cuit);
        setApellido(apellido);
        setNombre(nombre);
        setDireccion(direccion);
        setTelefono(telefono);
    }

    // Getters
    public String getCuit() {
        return this.cuit;
    }
    public String getApellido() {
        return this.apellido;
    }
    public String getNombre() {
        return this.nombre;
    }
    public String getDireccion() {
        return this.direccion;
    }
    public String getTelefono() {
        return this.telefono;
    }

    // Setters
    public boolean setCuit(String cuit) {
        if (!Util.validarCuit(cuit))
            return false;
        this.cuit = cuit;
        return true;
    }
    public boolean setApellido(String apellido) {
        if (apellido.equals("") || apellido.length() > 40)
            return false;
        this.apellido = apellido;
        return true;
    
    }
    public boolean setNombre(String nombre) {
        if (nombre.equals("") || nombre.length() > 40)
            return false;
        this.nombre = nombre;
        return true;
    }
    public boolean setDireccion(String direccion) {
        if (direccion.equals("") || direccion.length() > 30)
            return false;
        this.direccion = direccion;
        return true;
    }
    public boolean setTelefono(String telefono) {
        if (telefono.equals("") || telefono.length() > 20)
            return false;
        this.telefono = telefono;
        return true;
    }
    
    // Customs
    @Override
    public String toString() {
        return "CUIT      : " + getCuit()      + "\n" +
               "Apellido  : " + getApellido()  + "\n" +
               "Nombre    : " + getNombre()    + "\n" +
               "Direccion : " + getDireccion() + "\n" +
               "Telefono  : " + getTelefono();
    }
    
}
