package vista;

import java.awt.*;
import javax.swing.*;
import java.sql.*;
import java.awt.event.*;

import modelo.Cliente;
import modelo.ModeloTablaClientes;

import utileria.Util;

/**
 * Clase que hereda de JInternalFrame usada para mostrar y gestionar los Clientes de la aplicacion
 * (Altas y bajas por medio de JButtons, las modificaciones se realizan directamente sobre el JTable)
 * @author ISFT-189
 * @version 1.0
 */
class PanelClientes extends JInternalFrame implements ActionListener {

    private JButton btnAgregar;
    private JButton btnEliminar;

    private JTable tablaClientes;

    private GridBagConstraints gbc  = new GridBagConstraints();

    private ModeloTablaClientes MTC = null;

    private Connection cnx          = null;

    /**
     * Constructor
     * @param con referencia de tipo Connection a la conexion de la bbdd
     */
    public PanelClientes(Connection con){
        super("Clientes", true, false, true, true);

        this.cnx = con;
        
        this.setLayout( new GridBagLayout() );

        btnAgregar    = new JButton("Agregar");
        btnEliminar   = new JButton("Eliminar");
        
        MTC = new ModeloTablaClientes(this.cnx);
        
        // creando jtable con el modelo de Clientes
        tablaClientes = new JTable(MTC);
        tablaClientes.setToolTipText("Doble click en las celdas para modificar datos.");
        tablaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);;

        Util.agregarWidgetJC( this, gbc, btnAgregar     , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, btnEliminar    , 1, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, new JLabel("") , 2, 0, 1, 1, 1, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( this, gbc, new JScrollPane(tablaClientes), 0, 1, 4, 1, 1, 1, GridBagConstraints.BOTH );

        btnAgregar.addActionListener(this);
        btnEliminar.addActionListener(this);
    }

    /**
     * Metodo que establece el estado de algunos widgets.
     */
    public void estadoComponentes(boolean estado) {
        btnAgregar.setEnabled(estado);
        btnEliminar.setEnabled(estado);
        tablaClientes.setEnabled(estado);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAgregar) { // click btnAgregar
            new AgregarCliente(MTC);
        }
        if (e.getSource() == btnEliminar) { // click btnEliminar
            int indice = tablaClientes.getSelectedRow();
            if (indice != -1) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog (null, "¿Desea Eliminar el Cliente?\n" +
                                                             MTC.getListaClientes().get(indice), "Eliminar Cliente", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    if (MTC.eliminarCliente(indice))
                        Util.muestroMensaje("Cliente Eliminado");
                }
            }
        }
    }

}
