package modelo;

import java.sql.*;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

import utileria.Util;

/**
 * Clase que hereda a AbstractTableModel y es utilizada para representar un
 * modelo de tabla especifica de Clientes.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class ModeloTablaClientes extends AbstractTableModel {

    private ArrayList<Cliente> listaClientes = new ArrayList<Cliente>();

    private Connection cnx = null;

    private final String[] queryUpdate = {"UPDATE Cliente set cuit = '?' WHERE cuit = '!' ",
                                          "UPDATE Cliente set apellido = '?' WHERE cuit = '!' ",
                                          "UPDATE Cliente set nombre = '?' WHERE cuit = '!'",
                                          "UPDATE Cliente set direccion = '?' WHERE cuit = '!'",
                                          "UPDATE Cliente set telefono = '?' WHERE cuit = '!'"};
    /**
     * Constructor
     * @param con referencia de tipo Connection a la conexion de la bbdd
     */
    public ModeloTablaClientes(Connection con) {
        /*
        for(int x = 0 ; x < 10 ; x++) {
            getListaClientes().add( new Cliente() );
        }
        */
        this.cnx = con;
        try {
            ResultSet data = SimpleDataIO.getResultSet(this.cnx, "SELECT cuit, apellido, nombre, direccion, telefono FROM Cliente");
            while (data.next()) {
                getListaClientes().add( new Cliente(data.getString("cuit").trim(),
                                                   data.getString("apellido").trim(),
                                                   data.getString("nombre").trim(),
                                                   data.getString("direccion").trim(),
                                                   data.getString("telefono").trim() ) );
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
        }
    }

    // --------------------------------------------- Metodos obligatorios --------------------------------------------------
    
    @Override
    public Object getValueAt(int fila, int columna) {
        switch(columna) {
            case 0 : return getListaClientes().get(fila).getCuit();
            case 1 : return getListaClientes().get(fila).getApellido();
            case 2 : return getListaClientes().get(fila).getNombre();
            case 3 : return getListaClientes().get(fila).getDireccion();
            case 4 : return getListaClientes().get(fila).getTelefono();
        }
        return null;
    }
    
    @Override
    public int getColumnCount() {
        return 5;
    }
    
    @Override
    public int getRowCount() {
        return getListaClientes().size();
    }
    
    // ---------------------------------------------- Metodos adicionales -----------------------------------------------------
    
    @Override
    public boolean isCellEditable(int fila, int columna) {
       return true;
    }
    
    @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "CUIT";
            case 1 : return "Apellido";
            case 2 : return "Nombre";
            case 3 : return "Direccion";
            case 4 : return "Telefono";
        }
        return null;
    }
    
    @Override
    public Class getColumnClass(int columna) {
        switch(columna) {
            case 0 : return String.class;  // cuit
            case 1 : return String.class;  // apellido
            case 2 : return String.class;  // nombre
            case 3 : return String.class;  // direccion
            case 4 : return String.class;  // telefono
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int fila, int columna) {
        /*
        para actualizar las celdas :
           -verificar que el dato sea valido (en el caso de la columna cuit tmb verificar que no se repita)
             -dato ok, actualizar dato
        */
        String query;
        String cuitAnterior = getListaClientes().get(fila).getCuit();
        Cliente modCliente = new Cliente(getListaClientes().get(fila).getCuit(),
                                         getListaClientes().get(fila).getApellido(),
                                         getListaClientes().get(fila).getNombre(),
                                         getListaClientes().get(fila).getDireccion(),
                                         getListaClientes().get(fila).getTelefono() );
        switch(columna) {
            case 0:
                if (!modCliente.setCuit((String)aValue)) {
                    Util.muestroError("CUIT Incorrecto"); return;
                }
                // cuit ok, chequear que el cuit no este en la bbdd
                String queryAux = "SELECT cuit, apellido, nombre, direccion, telefono " +
                                  "FROM Cliente WHERE cuit = '" + modCliente.getCuit() + "' AND cuit  <> '" + cuitAnterior + "'";
                ResultSet data = SimpleDataIO.getResultSet(this.cnx, queryAux);
                try {
                    if (data.next()) {
                        Cliente clienteExistente = new Cliente(data.getString("cuit").trim(),
                                                               data.getString("apellido").trim(),
                                                               data.getString("nombre").trim(),
                                                               data.getString("direccion").trim(),
                                                               data.getString("telefono").trim() ); 
                        Util.muestroError("Cliente Existente : \n " + clienteExistente);
                        return;
                    }
                } catch (SQLException sqle) {
                    SimpleDataIO.showSQLException(sqle);
                    return;
                }
                 break;
            case 1:
                if (!modCliente.setApellido((String)aValue)) {
                    Util.muestroError("Apellido Incorrecto"); return;
                }
                 break;
            case 2:
                if (!modCliente.setNombre((String)aValue)) {
                    Util.muestroError("Nombre Incorrecto"); return;
                }
                break;
            case 3:
                if (!modCliente.setDireccion((String)aValue)) {
                    Util.muestroError("Direccion Incorrecta"); return;
                }
                break;
            case 4:
                if (!modCliente.setTelefono((String)aValue)) {
                    Util.muestroError("Telefono Incorrecto"); return;
                }
        }
        // seteo el query del update
        query = queryUpdate[columna].replace("?", aValue.toString()).replace("!", cuitAnterior);
        if (SimpleDataIO.executeQuery(this.cnx, query)) {
            SimpleDataIO.doCommit(this.cnx);
            getListaClientes().set(fila, modCliente);
            this.fireTableDataChanged();
            //Util.muestroMensaje("Cliente modificado");
        }
    }

    /**
     * Metodo que retorna el ArrayList con todos los Clientes.
     * @return listaClientes ArrayList<Cliente>
     */
    public ArrayList<Cliente> getListaClientes() {
        return listaClientes;
    }
    
    /**
     * Metodo que agrega un Cliente al sistema.
     * @param nuevoCliente es el nuevo cliente a dar de alta.
     * @return boolean true si se agrego correctamente, false en caso contrario.
     */
    public boolean agregarCliente(Cliente nuevoCliente) {
        //armor query de consulta para chequear si el cuit no esta dando de alta en bbdd
        String query;
        String queryAux = "SELECT cuit, apellido, nombre, direccion, telefono " + 
                          "FROM Cliente WHERE cuit = '" + nuevoCliente.getCuit() + "'";
        ResultSet data = SimpleDataIO.getResultSet(this.cnx, queryAux);
        try {
            if (data.next()) { // si existe fila entonces existe cliente
                Cliente clienteExistente = new Cliente(data.getString("cuit").trim(),
                                                       data.getString("apellido").trim(),
                                                       data.getString("nombre").trim(),
                                                       data.getString("direccion").trim(),
                                                       data.getString("telefono").trim() ); 
                Util.muestroError("Cliente Existente : \n " + clienteExistente);
                return true;
            }
        } catch (SQLException sqle) {
            SimpleDataIO.showSQLException(sqle);
            return false;
        }
        //CUIT disponible, puedo darlo de alta
        query = "INSERT INTO Cliente (cuit, apellido, nombre, direccion, telefono) values (" +
                       " '" + nuevoCliente.getCuit()      + "' , " +
                       " '" + nuevoCliente.getApellido()  + "' , " +
                       " '" + nuevoCliente.getNombre()    + "' , " +
                       " '" + nuevoCliente.getDireccion() + "' , " +
                       " '" + nuevoCliente.getTelefono()  + "' ) ";
        if (SimpleDataIO.executeQuery(this.cnx, query)) {
            SimpleDataIO.doCommit(this.cnx);
            getListaClientes().add(nuevoCliente);
            this.fireTableDataChanged();
            return true;
        } else return false;
    }
 
    /**
     * Metodo que elimina un Cliente del sistema tomando como referencia
     * el indice del cliente dentro de ListaClientes.
     * @param indice (de listaProductos) isndica el Producto a eliminar.
     * @return boolean true si se elimino correctamente, false en caso contrario.
     */
    public boolean eliminarCliente(int indice) {
        if (indice < 0 || indice > getListaClientes().size())
            return false;
        Cliente clienteEliminar = getListaClientes().get(indice);
        String query = "DELETE FROM Cliente where cuit = '" + clienteEliminar.getCuit() + "'";
        if (SimpleDataIO.executeQuery(this.cnx, query)) {
            SimpleDataIO.doCommit(this.cnx);
            getListaClientes().remove(indice);
            this.fireTableDataChanged();
            return true;
        } else return false;
    }

}