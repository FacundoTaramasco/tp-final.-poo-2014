package modelo;


/**
 * Clase simple POJO que representa un Producto.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class Producto {

    private int id;
    private String descripcion;
    private int cantidad;
    private double precioUnitario;
    
    // Constructor
    public Producto() {
        this(0);
    }
    public Producto(int id) {
        this(id, "Descripcion Default");
    }
    public Producto(int id, String descripcion) {
        this(id, descripcion, 0);
    }
    public Producto(int id, String descripcion, int cantidad) {
        this(id, descripcion, cantidad, 0.0);
    }
    public Producto(int id, String descripcion, int cantidad, double precio) {
        setId(id);
        setDescripcion(descripcion);
        setCantidad(cantidad);
        setPrecioUnitario(precio);
    }

    // Getters
    public int getId() {
        return this.id;
    }
    public String getDescripcion() {
        return this.descripcion;
    }
    public int getCantidad() {
        return this.cantidad;
    }
    public double getPrecioUnitario() {
        return this.precioUnitario;
    }
    
    // Setters
    public boolean setId(int id) {
        if (id < 0)
            return false;
        this.id = id;
        return true;
    }
    public boolean setDescripcion(String descripcion) {
        if (descripcion.equals("") || descripcion.length() > 60)
            return false;
        this.descripcion = descripcion;
        return true;
    }
    public boolean setCantidad(int cantidad) {
        if (cantidad < 0)
            return false;
        this.cantidad = cantidad;
        return true;
    }
    public boolean setPrecioUnitario(double precio) {
        if (precio < 0.0)
            return false;
        this.precioUnitario = precio;
        return true;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Producto) || obj == null)
            return false;
        if ( this.getId() == ((Producto)obj).getId() ) {
            //System.out.println("equals(Object obj) Producto");
            return true;
        }
        else return false;
    }
    
    // Customs
    @Override
    public String toString() {
        return "ID : "               + getId()          + "\n" +
               "Descripcion : "      + getDescripcion() + "\n" + 
               "Cantidad : "         + getCantidad()    + "\n" +
               "PrecioUnitario : $ " + getPrecioUnitario();
    }
}
