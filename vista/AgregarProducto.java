package vista;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import modelo.Producto;
import modelo.ModeloTablaProductos;

import utileria.Util;

/**
 * Clase que hereda de JDialog la cual se encarga de agregar
 * un Producto al sistema.
 * @author ISFT-189
 * @version 1.0
 */
public class AgregarProducto extends JDialog implements ActionListener {

    private JTextField txtId;
    private JTextField txtDescripcion;
    private JTextField txtCantidad;
    private JTextField txtPrecioUnitario;
    
    private JButton btnAceptar;
    private JButton btnCancelar;
    
    private GridBagConstraints gbc   = new GridBagConstraints();
    
    private ModeloTablaProductos MTP = null;

    /**
     * Constructor
     * @param MTP referencia al modelo tabla de productos.
     */
    public AgregarProducto(ModeloTablaProductos MTP) {
        
        this.MTP = MTP;
        
        this.getContentPane().setLayout(new GridBagLayout() );

        txtId             = new JTextField();
        txtDescripcion    = new JTextField();
        txtCantidad       = new JTextField();
        txtPrecioUnitario = new JTextField();
       
        btnAceptar        = new JButton("Aceptar");
        btnCancelar       = new JButton("Cancelar");
        
        Util.agregarWidgetCT( this, gbc, new JLabel("ID : ")              , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtId                            , 1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Descripcion : ")     , 0, 1, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtDescripcion                   , 1, 1, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Cantidad : ")        , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtCantidad                      , 1, 2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Precio Unitario : ") , 0, 3, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtPrecioUnitario                , 1, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        
        Util.agregarWidgetCT( this, gbc, btnAceptar                       , 0, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, btnCancelar                      , 1, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("")                   , 0, 5, 2, 1, 1, 1, GridBagConstraints.BOTH );

        this.setSize(340, 180);
        this.setLocationRelativeTo(this);
        this.setTitle("Agregar Producto");
        this.setModal(true);
 
        this.setResizable(false);
        
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        btnAceptar.addActionListener(this);
        btnCancelar.addActionListener(this);

        this.setVisible(true);
        /*
         when the JDialog is modal, then setVisible() blocks the current thread, i.e. the Dialog's Constructor. 
         Thus the Listeners are never added (actually only when the Dialog is closed). Therefore no events.
         Solution: set 'setVisible(true);' to the end of the CTOR
         */
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAceptar) {  /** click btnAceptar **/
            Producto nuevoProducto = new Producto();
            try {
                int idp      = Integer.parseInt(txtId.getText());
                String descp = txtDescripcion.getText();
                int cantp    = Integer.parseInt(txtCantidad.getText());
                double precp = Double.parseDouble(txtPrecioUnitario.getText());
                if (!nuevoProducto.setId(idp))               { Util.muestroError("Error Id", txtId); return; }
                if (!nuevoProducto.setDescripcion(descp))    { Util.muestroError("Error en Descripcin", txtDescripcion); return; }
                if (!nuevoProducto.setCantidad(cantp))       { Util.muestroError("Error en Cantidad", txtCantidad); return; }
                if (!nuevoProducto.setPrecioUnitario(precp)) { Util.muestroError("Error en Precio Unitario", txtPrecioUnitario); return; }
            } catch (NumberFormatException nef) {
                Util.muestroError("Valores incorrectos");
                return;
            }     
            if (MTP.agregarProducto(nuevoProducto)) {
                Util.muestroMensaje("Producto Agregado");
                dispose();
            }
        }
        if (e.getSource() == btnCancelar) { /** click btnCancelar **/
            dispose();
        }
    }
}
