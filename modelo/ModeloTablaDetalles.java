package modelo;

import java.sql.*;
import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.*;

import utileria.Util;

import vista.PanelFacturacion;

// En construccion...
/**
 * Clase que hereda a AbstractTableModel y es utilizada para representar el
 * modelo de lista de detalles de una Factura.
 * @author ISFT-189
 * @version 1.0
 */
public class ModeloTablaDetalles extends AbstractTableModel {

    private ArrayList<DetalleFactura> listaDetalles = null;
    
    private Factura miFac;
    
    private Connection cnx;
    
    private JTextField txtSubTotal;
    private JTextField txtTotalIva;
    private JTextField txtTotal;
    
    /**
     * Constructor
     * @param con referencia Connection a la bbdd.
     * @param listaD lista de los detalles de la factura.
     * @param miFactura referencia al modelo Factura
     * @param panelF referencia al panel de facturacion.
     */
    public ModeloTablaDetalles(Connection con, ArrayList<DetalleFactura> listaD, Factura miFactura, PanelFacturacion panelF) {
        this.cnx = con;
        this.listaDetalles = listaD; // utilizado para el momento de actualizar desde el jtable
        this.miFac = miFactura;      // utilizado para llamar metodos agregar, eliminar, modificar detalle y otros
        
        if (miFactura != null) {
            txtSubTotal = (JTextField)panelF.getWidgetMapa("txtSubTotal");
            txtTotalIva = (JTextField)panelF.getWidgetMapa("txtTotalIva");
            txtTotal    = (JTextField)panelF.getWidgetMapa("txtTotal");
        }
        /*for(int x = 0 ; x < 10 ; x++) {
            getListaDetalles().add( new DetalleFactura(x, "descripcion" + x, x, x, x) );
        }*/
    }

    // --------------------------------------------- Metodos obligatorios --------------------------------------------------

    @Override
    public Object getValueAt(int fila, int columna) {
        switch(columna) {
            case 0 : return getListaDetalles().get(fila).getId();
            case 1 : return getListaDetalles().get(fila).getDescripcion();
            case 2 : return getListaDetalles().get(fila).getCantidad();
            case 3 : return getListaDetalles().get(fila).getPrecioUnitario();
            case 4 : return getListaDetalles().get(fila).getImporte();
        }
        return null;
    }
    
    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public int getRowCount() {
        return listaDetalles.size();
    }

    // ---------------------------------------------- Metodos adicionales -----------------------------------------------------
    
    @Override
    public boolean isCellEditable(int fila, int columna) {
        switch(columna) {
            case 0 : return false; // id
            case 1 : return false; // descripcion
            case 2 : return true;  // cantidad
            case 3 : return true; // precio unitario
            case 4 : return false; // importe
        }
        return false;
    }
    
    @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "ID";
            case 1 : return "Descripcion";
            case 2 : return "Cantidad";
            case 3 : return "Precio Unitario";
            case 4 : return "Importe";
        }
        return null;
    }
    
    @Override
    public Class getColumnClass(int columna) {
        switch(columna) {
            case 0 : return Integer.class; // id
            case 1 : return String.class;  // descripcion
            case 2 : return Integer.class; // cantidad
            case 3 : return Double.class;  // precio unitario
            case 4 : return Double.class;  // importe
        }
        return null;
    }
    
    @Override
    public void setValueAt(Object aValue, int fila, int columna) {
        DetalleFactura detalleActual = getListaDetalles().get(fila);
        int cantidadActual;
        int cantidadNueva;
        double nuevoPrecioUnitario;
        switch (columna) {
            case 2 : // columna cantidad
                cantidadNueva = (Integer)aValue;
                try {
                    ResultSet data = SimpleDataIO.getResultSet(this.cnx, "SELECT cantidad FROM Producto WHERE id = " + detalleActual.getId());
                    data.next();
                    cantidadActual = data.getInt("cantidad");
                    if (cantidadNueva > cantidadActual) {
                        Util.muestroError("Stock NO Disponible");
                        return;
                    }
                    detalleActual.setCantidad(cantidadNueva);
                    if (miFac.modificarDetalle(detalleActual, fila)) {
                            actualizarValoresWidget();
                    }
                } catch (SQLException sqle) {
                    SimpleDataIO.showSQLException(sqle);
                }
                break;
            case 3 : // columna precioUnitario
                nuevoPrecioUnitario = (Double)aValue;
                detalleActual.setPrecioUnitario(nuevoPrecioUnitario);
                if (miFac.modificarDetalle(detalleActual, fila)) {
                    actualizarValoresWidget();
                }
        }
    }
    
    /**
     * Metodo que retorna el ArrayList con todos los detalles.
     * @return listaDetalles ArrayList<DetalleFactura>.
     */
    private ArrayList<DetalleFactura> getListaDetalles() {
        return listaDetalles;
    }

    /**
     * Metodo que limpia la lista de detalles.
     */
    public void limpiarLista() {
        miFac.limpiarLista();
        actualizarValoresWidget();
    }

    /**
     * Metodo que apartir de un DetalleFactura recibido lo agrega
     * a la lista si este no se encuentra previamente cargado.
     * @param nuevoDetalle es un DetalleFactura a ser agregado.
     * @return boolean true si se agrego correctamente, false en caso contrario.
     */
    public boolean agregarDetalle(DetalleFactura nuevoDetalle) {
        if (!miFac.agregarDetalle(nuevoDetalle))
            return false;
        actualizarValoresWidget();
        return true;
    }

    /**
     * Metodo que elimina un DetalleFactura de la lista tomando como referencia
     * el indice del Detalle dentro de ListaDetalles.
     * @param indice (de ListaDetalles) indica el Detalle a eliminar.
     * @return boolean true si se elimino correctamente, false en caso contrario.
     */
    public boolean eliminarDetalle(int indice) {
        if (!miFac.eliminarDetalle(indice))
            return false;
        actualizarValoresWidget();
        return true;
    }
    
    public void actualizarValoresWidget() {
        Double subT   = 0.0;
        Double monIva = 0.0;
        Double total  = 0.0;
        subT   = new Double(miFac.getSubTotal());
        monIva = new Double(miFac.getMontoIva());
        total  = new Double(miFac.getTotal());
        txtSubTotal.setText(subT.toString());
        txtTotalIva.setText(monIva.toString());
        txtTotal.setText(total.toString());
        this.fireTableDataChanged();
    }
}
