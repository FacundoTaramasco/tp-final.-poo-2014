package vista;

import java.util.*;
import java.sql.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

import utileria.Util;

import modelo.Cliente;
import modelo.Factura;
import modelo.DetalleFactura;
import modelo.SimpleDataIO;
import modelo.ModeloTablaDetalles;
import modelo.ModeloTablaFacturas;

import modelo.GenerarPDF;


/**
 * Clase que hereda de JDialog usada para mostrar todas las Facturas y sus respectivos
 * detalles del sistema.
 * @author ISFT-189
 * @version 1.0
 */
public class ListadoFacturas extends JDialog {

    private JButton btnGenerarPDF;
    private JTable tablaFacturas;
    private JTable tablaDetalles;
    private JButton btnCerrar;
    
    private GridBagConstraints gbc  = new GridBagConstraints();
    
    private ModeloTablaFacturas MTF = null;
    private ModeloTablaDetalles MTD = null;
    
    private Factura facturaActual   = null;
    private Connection cnx;
    
    /**
     * Constructor
     * @param con parametro connection a la bbdd.
     */
    public ListadoFacturas(Connection con) {
        this.cnx = con;

        this.getContentPane().setLayout(new GridBagLayout());
        this.setTitle("Listado de Facturas");

        btnGenerarPDF = new JButton("Generar PDF");
        
        MTF           = new ModeloTablaFacturas(con);
        tablaFacturas = new JTable(MTF);
        tablaFacturas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tablaDetalles = new JTable();
        tablaDetalles.setEnabled(false);
        btnCerrar     = new JButton("   Cerrar   ");

        Util.agregarWidgetCT( this, gbc, btnGenerarPDF                  , 1, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, new JLabel("")                 , 0, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JScrollPane(tablaFacturas) , 0, 1, 2, 1, 1, 1, GridBagConstraints.BOTH );
        Util.agregarWidgetCT( this, gbc, new JScrollPane(tablaDetalles) , 0, 2, 2, 1, 1, 1, GridBagConstraints.BOTH );
        Util.agregarWidgetCT( this, gbc, new JLabel("")                 , 0, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, btnCerrar                      , 1, 3, 1, 1, 0, 0, GridBagConstraints.NONE );

        this.setModal(true);
        this.setSize(700, 500);
        this.setLocationRelativeTo(this);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        /** Clase anonima que gestiona evento 'click' en btnCerrar **/
        btnCerrar.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == btnCerrar)
                    dispose();
            }
        });

        /** Clase anonima que gestiona el evento 'valueChanged' de JTable facturas  **/
        tablaFacturas.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent event) {
                int numero           = (Integer)tablaFacturas.getValueAt(tablaFacturas.getSelectedRow(), 0);
                String cuit          = (String)tablaFacturas.getValueAt(tablaFacturas.getSelectedRow(), 1);
                java.util.Date fecha = (java.util.Date)tablaFacturas.getValueAt(tablaFacturas.getSelectedRow(), 2);
                double iva           = (Double)tablaFacturas.getValueAt(tablaFacturas.getSelectedRow(), 3);
                double monto         = (Double)tablaFacturas.getValueAt(tablaFacturas.getSelectedRow(), 4);
                ResultSet data       = null;
                Cliente cli          = null;
                ArrayList<DetalleFactura> listaD = new ArrayList<DetalleFactura>();
                
                // query de consulta para obtener todos los detalles de la factura seleccionada
                String queryConsulta = "SELECT " +
                                        "d.idprod ," +
                                        "p.descripcion, " +
                                        "d.cantidad, " +
                                        "d.precio  " +
                                        "FROM producto p " +
                                        "INNER JOIN detalle d " +
                                        "ON d.idprod = p.id " +
                                        "AND d.nrofac = " + numero;

                try {
                    data = SimpleDataIO.getResultSet(cnx, "SELECT cuit, apellido, nombre, direccion, telefono " +
                                                           "FROM Cliente where cuit = '" + cuit + "'");
                    data.next();
                    // obtengo el cliente de la factura
                    cli = new Cliente(cuit, data.getString("apellido").trim(),
                                            data.getString("nombre").trim(),
                                            data.getString("direccion").trim(),
                                            data.getString("telefono").trim());
                    data = SimpleDataIO.getResultSet(cnx, queryConsulta);
                    // obtengo todos los detalles de la factura
                    while (data.next()) {
                       listaD.add( new DetalleFactura(data.getInt("idprod"),
                                                      data.getString("descripcion").trim(),
                                                      data.getInt("cantidad"),
                                                      data.getDouble("precio")) );
                    }
                    MTD = new ModeloTablaDetalles(null, listaD, null, null);
                    // muestro todos los detalles en JTable detalles, estableciendo el model
                    tablaDetalles.setModel(MTD);
                    // llegado el caso de que se quiera generar el pdf ya tengo el modelo de factura
                    facturaActual = new Factura(listaD, numero, cli, fecha, iva, monto);
                    //System.out.println(facturaActual);
                } catch (SQLException sqle) {
                    SimpleDataIO.showSQLException(sqle);
                }
            }
        });
 
        /** clase anonima evento 'click' en btnGenerarPDF **/
        btnGenerarPDF.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (tablaFacturas.getSelectedRow() == -1)
                    Util.muestroError("Debe seleccionar una factura.");
                if (facturaActual != null) {
                    GenerarPDF gpdf = new GenerarPDF(facturaActual);
                    if (!gpdf.realizarPDF())
                        Util.muestroError("Error al generar pdf");
                    else
                        Util.muestroMensaje("PDF Generado Correctamente");
                        
                }
            }
        });

        this.setVisible(true);
    }
}
