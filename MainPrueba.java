
import modelo.Producto;
import modelo.Cliente;
import modelo.DetalleFactura;
import modelo.Factura;

import modelo.GenerarPDF;
import java.util.*;


/**
 * Clase de prueba..
 * 
 * @author ISTF-189
 * @version 1.0
 */
public class MainPrueba {

    public static void main(String[] args) {
        Producto p1 = new Producto(1, "desc1", 5, 563.35);
        System.out.println("Producto p1 creado : \n" + p1);
        Cliente c1 = new Cliente("27111111119");
        System.out.println("Cliente c1 creado : \n" + c1);
        DetalleFactura df = new DetalleFactura(1, "desc1", 5, 563.35, 123);
        System.out.println("DetalleFactura df : \n" + df +"\n\n");
        
        
        ArrayList<DetalleFactura> listaDetalles = new ArrayList<DetalleFactura>();
        DetalleFactura dt1 = new DetalleFactura(1,"lenovo", 4, 500);
        DetalleFactura dt4 = new DetalleFactura(4,"fgfgfg", 8, 4545);
        DetalleFactura dt3 = new DetalleFactura(3,"fgfgfg", 8, 4545);
        DetalleFactura dt6 = new DetalleFactura(6,"fgfgfg", 8, 4545);
        DetalleFactura dt7 = new DetalleFactura(7,"fgfgfg", 8, 4545);
        DetalleFactura dt8 = new DetalleFactura(8,"fgfgfg", 8, 4545);
        DetalleFactura dt9 = new DetalleFactura(9,"fgfgfg", 8, 4545);
        DetalleFactura dt2 = new DetalleFactura(1,"lenovo443", 0, 0, 0);
        
        /*
        ArrayList<Producto> listaProducto = new ArrayList<Producto>();
        listaProducto.add(p1);
        System.out.println("posicion de p1 : " + listaProducto.indexOf(new Producto(2)));
        */
        
        if(!listaDetalles.contains(dt1))
            listaDetalles.add(dt1);
        
        listaDetalles.add(dt1);
        listaDetalles.add(dt4);
        listaDetalles.add(dt3);
        listaDetalles.add(dt6);
        listaDetalles.add(dt7);
        listaDetalles.add(dt8);
        listaDetalles.add(dt9);
        if(!listaDetalles.contains(dt2))
            listaDetalles.add(dt2);
        System.out.println("posicion de dt9 : " + listaDetalles.indexOf(new DetalleFactura(9)));
        
        /*
        DetalleFactura otroDetalle = new DetalleFactura(10,"HP", 1, 1500);
        System.out.println(otroDetalle);
        otroDetalle.setCantidad(3);
        System.out.println(otroDetalle);
        otroDetalle.setPrecioUnitario(1000);
        System.out.println(otroDetalle);
        */
       
        /*
        Factura fac = new Factura(null);
        GenerarPDF gpdf = new GenerarPDF(fac);
        gpdf.realizarPDF();
        */
    }
}
