package modelo;

// En Construccion...
/**
 * Clase simple POJO que representa un Detalle de Factura.
 * 
 * @author ISFT-189
 * @version 1.0
 */
public class DetalleFactura extends Producto {

    private double importe;
    
    /**
     * Constructor
     */
    public DetalleFactura() {
        this(0);
    }
    public DetalleFactura(int id) {
        this(id, "Descripcion Default");
    }
    public DetalleFactura(int id, String descripcion) {
        this(id, descripcion, 0);
    }
    public DetalleFactura(int id, String descripcion, int cantidad) {
        this(id, descripcion, cantidad, 0.0);
    }
    public DetalleFactura(int id, String descripcion, int cantidad, double precio) {
        this(id, descripcion, cantidad, precio, cantidad * precio);
    }
    public DetalleFactura(int id, String descripcion, int cantidad, double precio, double importe) {
        super(id, descripcion, cantidad, precio);
        setImporte(importe);
    }
    
    // Getters
    public double getImporte() {
        return this.importe;
    }
    
    // Setters
    private boolean setImporte(double importe) {
        if (importe < 0.0)
            return false;
        this.importe = importe;
        return true;
    }
    
    
    /* Al cambiar la cantidad o el precioUnitario se calcula el nuevo valor del importe. */
 
    @Override
    public boolean setCantidad(int cant) {
        if (!super.setCantidad(cant))
            return false;
        setImporte(calcularImporte());
        return true;
    }
    
    @Override
    public boolean setPrecioUnitario(double precio) {
        if (!super.setPrecioUnitario(precio))
            return false;
        setImporte(calcularImporte());
        return true;
    }
    
    // Customs
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DetalleFactura) || obj == null)
            return false;
        if ( this.getId() == ((DetalleFactura)obj).getId() ) {
            //System.out.println("equals(Object obj) DetalleFactura");
            return true;
        }
        else return false;
    }
    
    private double calcularImporte() {
        return getCantidad() * getPrecioUnitario();
    }
        
    public String toString() {
        return super.toString() + "\n" +
        "Importe : $ " + getImporte();
    }
}
