package vista;

import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import java.beans.PropertyVetoException;

import modelo.SimpleDataIO;


/**
 * Interfaz grafica principal de la aplicacion que gestiona Productos, 
 * Clientes y Facturacion contra una BBDD postgresql.
 * @author ISFT-189
 * @version 1.0
 */
public class GUIMain extends JFrame {

    private JMenuBar  barra;
    private JMenu     optBarra;
    private JMenuItem itmProd;
    private JMenuItem itmCli;
    private JMenuItem itmFac;
    private JMenuItem itmCerrar;

    private JDesktopPane JDP;
    
    private PanelProductos PanelP   = null;
    private PanelClientes PanelC    = null;
    private PanelFacturacion PanelF = null;

    private Connection cnxBBDD      = null;
    
    /**
     * Constructor
     */
    public GUIMain() {
        super("TP-Final-POO");

        // estableciendo conexion con la bbdd
        String urlBBDD = "jdbc:postgresql://localhost:5432/TP-Final-POO";
        String user    = "postgres";
        String psswd   = "masterkey";
        this.cnxBBDD = SimpleDataIO.getConnection(urlBBDD, user, psswd);
        
        this.getContentPane().setLayout( new BorderLayout() );
   
        barra     = new JMenuBar();
        optBarra  = new JMenu("Opciones");
        itmProd   = new JMenuItem("Productos");
        itmCli    = new JMenuItem("Clientes");
        itmFac    = new JMenuItem("Facturacion");
        itmCerrar = new JMenuItem("Cerrar");

        optBarra.add(itmProd);
        optBarra.add(itmCli);
        optBarra.add(itmFac);
        optBarra.add(itmCerrar);
        barra.add(optBarra);

        this.setJMenuBar(barra);

        // crear y establecer JDesktopPane
        JDP = new JDesktopPane();
        //JDP.setDragMode(JDesktopPane.OUTLINE_DRAG_MODE);
        
        this.add(JDP, "Center");
        this.add( new JLabel("  "), "South"); //bottom bar de prueba!

        ControlOpciones co = new ControlOpciones();
        itmProd.addActionListener(co);
        itmCli.addActionListener(co);
        itmFac.addActionListener(co);
        itmCerrar.addActionListener(co);

        PanelP   = new PanelProductos(cnxBBDD);
        PanelC   = new PanelClientes(cnxBBDD);
        PanelF   = new PanelFacturacion(cnxBBDD, PanelP, PanelC);
    
        // adjuntar marcos internos al JDesktopPane y mostrarlos
        JDP.add(PanelP);
        PanelP.setSize(550, 320);
        PanelP.setLocation(10, 10);
        PanelP.setVisible(true);

        JDP.add(PanelC);
        PanelC.setSize(550, 320);
        PanelC.setLocation(10, 335);
        PanelC.setVisible(true);

        JDP.add(PanelF);
        PanelF.setSize(790, 645);
        PanelF.setLocation(565, 10);
        PanelF.setVisible(true);

        //this.setSize(1250, 680);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setVisible(true);
        
        /** Clase anonima evento cerrar ventana **/
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                SimpleDataIO.finish(cnxBBDD);
                System.exit(0);
            }
        });
    }

    /**
    Clase interna que implementa ActionListener, usada para escuchar eventos del
    widget JMenuItem.
    */
    private class ControlOpciones implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e)  {
            if (e.getSource() == itmCerrar) {         /** click menuitem cerrar **/
                SimpleDataIO.finish(cnxBBDD);
                System.exit(0);
            }
            if (e.getSource() == itmProd) {           /** click menuitem producto **/
                try {
                    PanelP.setIcon(false);
                    PanelP.toFront();
                    PanelP.setSelected(true);
                } catch (PropertyVetoException pve) {
                    return;
                }
            }
            if (e.getSource() == itmCli) {            /** click menuitem cliente **/
                try {
                    PanelC.setIcon(false);
                    PanelC.toFront();
                    PanelC.setSelected(true);
                } catch (PropertyVetoException pve) {
                    return;
                }
            }
            if (e.getSource() == itmFac) {            /** click  menuitem facturacion **/
                try {
                    PanelF.setIcon(false);
                    PanelF.toFront();
                    PanelF.setSelected(true);
                } catch (PropertyVetoException pve) {
                    return;
                }
            }
        }
    }
}