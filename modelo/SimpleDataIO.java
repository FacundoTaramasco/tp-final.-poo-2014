package modelo;

import java.sql.*;
/**
 * Clase simple que me conecta con Postgresql
 * Similar a DataIO, pero mas simple
 * 
 * @author G. Cherencio
 * @version 1.0
 */
public class SimpleDataIO {

    private static  DatabaseMetaData dbMetaData = null;

    public static Connection getConnection(String db,String usr,String pwd) {
        boolean transactionSupported = false;
        Connection c = null;
       
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(db, usr, pwd);
            System.out.println ("Connection established.");
            c.setAutoCommit(false);
            System.out.println("Auto-commit is disabled.");
            SimpleDataIO.dbMetaData = c.getMetaData();
            // Ok, let's query a driver/database capability
            if (dbMetaData.supportsTransactions()) {
                System.out.println ("Transactions are supported.");
                transactionSupported = true;
            } else {
                System.out.println ("Transactions are not supported.");
                transactionSupported = false;
            }
            return c;
        } catch (SQLException sqe) {
            showSQLException(sqe);
            return null;
        } catch(ClassNotFoundException cnfe) {
            //showSQLException(cnfe);
            return null;
        }
    }
    
    public static ResultSet getResultSet(Connection c,String query) {
        // estoy conectado!, por lo tanto, puedo hacer query!
        Statement s  = null;
        ResultSet rs = null;
        try {
            // creo statement
            s = c.createStatement();
            // creo resultset
            rs = s.executeQuery(query);
            System.out.println(query);
        } catch(SQLException e) {
            //e.printStackTrace();
            //System.out.println ("Error al hacer query sobre la base de datos.");
            showSQLException(e);
        }
        return rs;
    }
    
    public static boolean doCommit(Connection c) {
        // estoy conectado!, por lo tanto, puedo hacer query!
        boolean ret = true;
        try {
            // creo statement
            c.commit();
        } catch(SQLException e) {
            //e.printStackTrace();
            //System.out.println ("Error al hacer query update sobre la base de datos.");
            showSQLException(e);
            ret = false;
        } 
        return ret;
    }
    
    public static boolean executeQuery(Connection c,String query) {
        // estoy conectado!, por lo tanto, puedo hacer query!
        Statement s = null;
        boolean ret = true;
        try {
            // creo statement
            s = c.createStatement();
            // creo resultset
            s.executeUpdate(query);
            System.out.println(query);
        } catch(SQLException e) {
            //e.printStackTrace();
            //System.out.println ("Error al hacer query update sobre la base de datos.");
            showSQLException(e);
            ret=false;
        }
        return ret;
    }
    
    // Display an SQLException which has occured in this application.
    public static void showSQLException (SQLException e) {
        // Notice that a SQLException is actually a chain of SQLExceptions,
        // let's not forget to print all of them...
        SQLException next = e;
        while (next != null) {
            System.out.println (next.getMessage ());
            System.out.println ("Error Code: " + next.getErrorCode ());
            System.out.println ("SQL State: " + next.getSQLState ());
            next = next.getNextException ();
        }
    }

    /**
     * Metodo que cierra la conexion con la base de datos
     */
    public static void finish(Connection c) {
        if ( c == null ) return;
        try {
            c.close();
            System.out.println ("Connection closed.");
        } catch (java.sql.SQLException e) {
            showSQLException(e);
        }        
    }
  
}
