package utileria;


import javax.swing.*;
import java.awt.*;

/**
 * Clase de utileria de la aplicacion.
 * @author ISFT-189
 * @version 1.0
 */
public class Util {

    // En construccion...

    public static void agregarWidgetCT( Container JF, GridBagConstraints gbc,
                            JComponent widget, int gridx, int gridy, int gridwidth, 
                              int gridheight , int weightx, int weighty, int fill ) {
        gbc.gridx      = gridx;      // en que columna
        gbc.gridy      = gridy;      // en que fila
        gbc.gridwidth  = gridwidth;  // ancho de celda
        gbc.gridheight = gridheight; // alto de celda
        gbc.weightx    = weightx;    // cuanto crece en ancho al redimensionarse
        gbc.weighty    = weighty;    // cuanto crece en alto al redimensionarse
        gbc.fill       = fill;       // comportamiento al crecer
        JF.add( widget, gbc ); // agrega widget al JIF con las restricciones establecidas
    }
    
    public static void agregarWidgetJC( JComponent JC, GridBagConstraints gbc,
                            JComponent widget, int gridx, int gridy, int gridwidth, 
                              int gridheight , int weightx, int weighty, int fill ) {
        gbc.gridx      = gridx;      // en que columna
        gbc.gridy      = gridy;      // en que fila
        gbc.gridwidth  = gridwidth;  // ancho de celda
        gbc.gridheight = gridheight; // alto de celda
        gbc.weightx    = weightx;    // cuanto crece en ancho al redimensionarse
        gbc.weighty    = weighty;    // cuanto crece en alto al redimensionarse
        gbc.fill       = fill;       // comportamiento al crecer
        JC.add( widget, gbc ); // agrega widget al JIF con las restricciones establecidas
    }

    public static boolean muestroError(final String msg) {
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE); 
        return false;
    }
    
    public static boolean muestroError(final String msg,final JComponent comp) {
        JOptionPane.showMessageDialog(null, msg, "Error", JOptionPane.ERROR_MESSAGE); 
        comp.requestFocus();
        return false;
    }

    public static void muestroMensaje(final String msg) {
        JOptionPane.showMessageDialog(null, msg, "Mensaje", JOptionPane.INFORMATION_MESSAGE); 
    }
    
    public static boolean validarCuit(String cuit) {
        if (cuit.length() != 11)
            return false;
        if (!cuit.substring(0, 2).equals("20") && !cuit.substring(0, 2).equals("27") && !cuit.substring(0, 2).equals("30"))
            return false;
        return true;
    }
    
    public static double redondeo(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
    
    public static String seteoCampoDetalle(String str, int largo) {
        //quito espacios de sobra por izq. y der.
        StringBuffer sb = new StringBuffer(str.trim());
        //si el largo de la cadena es menor a 'largo' caracteres le agrego espacios hacia la derecha
        for (int i = sb.length() ; i < largo ; i++)
            sb.insert(i," ");
        String slok = new String(sb.substring(0, largo));//solo me interesan los primeros 'largo' caracteres
        return slok;

    }
}
