package modelo;

import java.sql.*;
import java.util.*;
import javax.swing.table.*;

/**
 * Clase que hereda a AbstractTableModel y es utilizada para mostrar
 * una lista completa de todas las facturas del sistema.
 * @author ISFT-189
 * @version 1.0
 */
public class ModeloTablaFacturas extends AbstractTableModel {
    
    private ArrayList<Factura> listaFacturas = new ArrayList<Factura>();
    
    private Connection cnx;
    /**
     * Constructor
     * @param con referencia tipo Connection a la bbdd.
     */
    public ModeloTablaFacturas(Connection con) {
        this.cnx = con;
        try {
            ResultSet data = SimpleDataIO.getResultSet(cnx, "SELECT numero, cuitcliente, fecha, iva, monto from Factura");
            while (data.next()) {
                getListaFacturas().add( new Factura(null, data.getInt("numero"),
                                                new Cliente(data.getString("cuitcliente").trim()),
                                                (java.util.Date)data.getDate("fecha"),
                                                data.getDouble("iva"),
                                                data.getDouble("monto") )  );
            }
        } catch (SQLException sqle) {            
        
        }
    }
    
    @Override
    public Object getValueAt(int fila, int columna) {
        switch (columna) {
            case 0 : return getListaFacturas().get(fila).getNumero();
            case 1 : return getListaFacturas().get(fila).getCliente().getCuit();
            case 2 : return getListaFacturas().get(fila).getFecha();
            case 3 : return getListaFacturas().get(fila).getValorIva();
            case 4 : return getListaFacturas().get(fila).getTotal();
        }
        return null;
    }
    
    @Override
    public int getColumnCount() {
        return 5;
    }
    
    @Override
    public int getRowCount() {
        return getListaFacturas().size();
    }
    
     @Override
    public String getColumnName(int columna) {
        switch(columna) {
            case 0 : return "Numero";
            case 1 : return "Cuit Cliente";
            case 2 : return "Fecha";
            case 3 : return "IVA";
            case 4 : return "Monto";
        }
        return null;
    }
    
    
    public ArrayList<Factura> getListaFacturas() {
        return this.listaFacturas;
    }
}
