package vista;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;

import modelo.DetalleFactura;
import modelo.ModeloTablaDetalles;
import modelo.SimpleDataIO;

import utileria.Util;

/**
 * Clase que hereda de JDialog la cual se encarga de agregar
 * un DetalleFactura a Factura.
 * @author ISFT-189
 * @version 1.0
 */
public class AgregarDetalle extends JDialog implements ActionListener {
    
    private JTextField txtId;
    private JTextField txtCantidad;

    private JButton btnAceptar;
    private JButton btnCancelar;
    
    private GridBagConstraints gbc   = new GridBagConstraints();

    private Connection cnx;
    private ModeloTablaDetalles MTD;

    /**
     * Constructor
     * @param con referencia Connection de la bbdd.
     * @param MTD referencia al modelo tabla detalles.
     */
    public AgregarDetalle(Connection con, ModeloTablaDetalles MTD) {
        this.cnx = con;
        this.MTD = MTD;
        
        this.getContentPane().setLayout(new GridBagLayout() );

        txtId        = new JTextField();
        txtCantidad  = new JTextField();
    
        btnAceptar   = new JButton("Aceptar");
        btnCancelar  = new JButton("Cancelar");
        
        Util.agregarWidgetCT( this, gbc, new JLabel("ID : ")       , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtId                     , 1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetCT( this, gbc, new JLabel("Cantidad : ") , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtCantidad               , 1, 2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetCT( this, gbc, btnAceptar     , 0, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, btnCancelar    , 1, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("") , 0, 4, 2, 1, 1, 1, GridBagConstraints.BOTH );
        
        this.setSize(340, 150);
        this.setLocationRelativeTo(this);
        this.setTitle("Agregar Detalle Factura");
        this.setModal(true);
        
        this.setResizable(false);
        
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        btnAceptar.addActionListener(this);
        btnCancelar.addActionListener(this);
        
        this.setVisible(true);
        /*
         when the JDialog is modal, then setVisible() blocks the current thread, i.e. the Dialog's Constructor. 
         Thus the Listeners are never added (actually only when the Dialog is closed). Therefore no events.
         Solution: set 'setVisible(true);' to the end of the CTOR
         */
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAceptar) { /** click btnAceptar **/
            DetalleFactura nuevoDetalle = new DetalleFactura();
            try {
                int idProd = Integer.parseInt(txtId.getText());
                int cantp  = Integer.parseInt(txtCantidad.getText());
                if (!nuevoDetalle.setId(idProd))      { Util.muestroError("Valor Incorrecto Id", txtCantidad); return; }
                if (!nuevoDetalle.setCantidad(cantp)) { Util.muestroError("Valor Incorrecto Cantidad", txtCantidad); return; }
                ResultSet data = SimpleDataIO.getResultSet(this.cnx, "SELECT id, descripcion, cantidad, precioUnitario " + 
                                                                     "FROM Producto WHERE id = " + idProd);
                if (!data.next()) {
                    Util.muestroError("Id inexistente", txtCantidad);
                    return;
                } 
                    
                if (cantp > data.getInt("cantidad")) {
                    Util.muestroError("Stock NO Disponible", txtCantidad);
                    return;
                }

                nuevoDetalle.setId(data.getInt("id"));
                nuevoDetalle.setDescripcion(data.getString("descripcion").trim());
                //nuevoDetalle.setCantidad(data.getInt("cantidad"));
                nuevoDetalle.setPrecioUnitario(data.getDouble("precioUnitario"));
                //nuevoDetalle.setImporte( cantp * data.getDouble("precioUnitario") );
                
                /*
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog (null, "¿Desea Agregar Detalle?\n" +
                                                                 nuevoDetalle, "Agregar Detalle", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    
                }
                */
                if (MTD.agregarDetalle(nuevoDetalle) ) {
                    Util.muestroMensaje("Detalle Agregado");
                    dispose();
                } else
                    Util.muestroError("No puede Agregar Detalle");
                
            } catch(NumberFormatException nfe) {
                Util.muestroError("Datos Invalidos");
                return;
            } catch(SQLException sqle) {
                SimpleDataIO.showSQLException(sqle);
                return;
            }
        }
        if (e.getSource() == btnCancelar) {  /** click btnCancelar **/
            dispose();
        }
    }
    
}