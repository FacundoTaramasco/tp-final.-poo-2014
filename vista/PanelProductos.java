package vista;

import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import modelo.Producto;
import modelo.ModeloTablaProductos;

import utileria.Util;

/**
 * Clase que hereda de JInternalFrame usada para mostrar y gestionar los Productos de la aplicacion
 * (Altas y bajas por medio de JButtons, las modificaciones se realizan directamente sobre las celdas del JTable)
 * @author ISFT-189
 * @version 1.0
 */
class PanelProductos extends JInternalFrame implements ActionListener {

    private JButton btnAgregar;
    private JButton btnEliminar;

    public JTable tablaProducto;

    private GridBagConstraints gbc = new GridBagConstraints();

    private ModeloTablaProductos MTP = null;

    private Connection cnx;

    /**
     * Constructor
     * @param con referencia de tipo Connection a la conexion de la bbdd
     */
    public PanelProductos(Connection con) {
        super("Productos", true, false, true, true);

        this.cnx = con;
        
        this.setLayout( new GridBagLayout() );

        btnAgregar    = new JButton("Agregar");
        btnEliminar   = new JButton("Eliminar");

        MTP = new ModeloTablaProductos(cnx);
        
        // creando jtable con el modelo de Productos
        tablaProducto = new JTable(MTP);
        tablaProducto.setToolTipText("Doble click en las celdas para modificar datos.");
        tablaProducto.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JPanel panelBotones = new JPanel( new GridBagLayout() );
        //panelBotones.setBorder(BorderFactory.createLineBorder(Color.black));
        
        Util.agregarWidgetJC( panelBotones, gbc, btnAgregar        , 0, 0, 1, 0, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelBotones, gbc, btnEliminar       , 1, 0, 1, 0, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetJC( panelBotones, gbc, new JLabel("")    , 2, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetJC( this, gbc, panelBotones                   , 0, 0, 1, 1, 0, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetJC( this, gbc, new JScrollPane(tablaProducto) , 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH );

        btnAgregar.addActionListener(this);
        btnEliminar.addActionListener(this);
    }

    /**
     * Metodo que establece el estado de algunos widgets.
     * @param estado boolean del estado a setear a los widgets.
     */
    public void estadoComponentes(boolean estado) {
        btnAgregar.setEnabled(estado);
        btnEliminar.setEnabled(estado);
        tablaProducto.setEnabled(estado);
    }
    
    public ModeloTablaProductos getMTP() {
        return this.MTP;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAgregar) {  // click btnAgregar
            new AgregarProducto(MTP);
        }
        if (e.getSource() == btnEliminar) { // click btnEliminar
            int indice = tablaProducto.getSelectedRow();
            if ( indice != -1) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog (null, "¿Desea Eliminar el Producto?\n" +
                                                             MTP.getListaProductos().get(indice), "Eliminar Producto", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    if (MTP.eliminarProducto(indice))
                        Util.muestroMensaje("Producto Eliminado");
                }
            } 
        }
    }
}