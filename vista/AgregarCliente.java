package vista;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

import modelo.Cliente;
import modelo.ModeloTablaClientes;

import utileria.Util;

/**
 * Clase que hereda de JDialog utilizada para agregar un Cliente
 * al sistema.
 * @author ISFT-189
 * @version 1.0
 */
public class AgregarCliente extends JDialog implements ActionListener {

    private JTextField txtCuit;;
    private JTextField txtApellido;
    private JTextField txtNombre;
    private JTextField txtDireccion;
    private JTextField txtTelefono;
    
    private JButton btnAceptar;
    private JButton btnCancelar;
    
    private GridBagConstraints gbc = new GridBagConstraints();
    
    private ModeloTablaClientes MTC = null;

    /**
     * Constructor
     * @param MTC referencia al modelo tabla de clientes,
     */
    public AgregarCliente(ModeloTablaClientes MTC) {
        
        this.MTC = MTC;
        
        this.getContentPane().setLayout(new GridBagLayout() );

        txtCuit       = new JTextField();
        txtApellido   = new JTextField();
        txtNombre     = new JTextField();
        txtDireccion  = new JTextField();
        txtTelefono   = new JTextField();
       
        btnAceptar    = new JButton("Aceptar");
        btnCancelar   = new JButton("Cancelar");
        
        Util.agregarWidgetCT( this, gbc, new JLabel("CUIT : ")      , 0, 0, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtCuit                    , 1, 0, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Apellido : ")  , 0, 1, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtApellido                , 1, 1, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Nombre : ")    , 0, 2, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtNombre                  , 1, 2, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Direccion : ") , 0, 3, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtDireccion               , 1, 3, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("Telefono : ")  , 0, 4, 1, 1, 0, 0, GridBagConstraints.NONE );
        Util.agregarWidgetCT( this, gbc, txtTelefono                , 1, 4, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );

        Util.agregarWidgetCT( this, gbc, btnAceptar        , 0, 5, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, btnCancelar       , 1, 5, 1, 1, 1, 0, GridBagConstraints.HORIZONTAL );
        Util.agregarWidgetCT( this, gbc, new JLabel("")    , 0, 7, 2, 1, 1, 1, GridBagConstraints.BOTH );

        this.setSize(340, 200);
        this.setLocationRelativeTo(this);
        this.setTitle("Agregar Cliente");
        this.setModal(true);
        
        this.setResizable(false);
        
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        btnAceptar.addActionListener(this);
        btnCancelar.addActionListener(this);

        this.setVisible(true);
        /*
         when the JDialog is modal, then setVisible() blocks the current thread, i.e. the Dialog's Constructor. 
         Thus the Listeners are never added (actually only when the Dialog is closed). Therefore no events.
         Solution: set 'setVisible(true);' to the end of the CTOR
         */
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnAceptar) {  /** click btnAceptar **/
            Cliente nuevoCli = new Cliente();
            String ct  = txtCuit.getText().trim();
            String ape = txtApellido.getText().trim();
            String nom = txtNombre.getText().trim();
            String dir = txtDireccion.getText().trim();
            String tel = txtTelefono.getText().trim();
            if (!nuevoCli.setCuit(ct))       { Util.muestroError("Error en CUIT", txtCuit); return; }
            if (!nuevoCli.setApellido(ape))  { Util.muestroError("Error en Apellido", txtApellido); return; }
            if (!nuevoCli.setNombre(nom))    { Util.muestroError("Error en Nombre", txtNombre); return; }
            if (!nuevoCli.setDireccion(dir)) { Util.muestroError("Error en Direccion", txtDireccion); return; }
            if (!nuevoCli.setTelefono(tel))  { Util.muestroError("Error en Telefono", txtTelefono); return; }
            if (MTC.agregarCliente(nuevoCli)) {
                Util.muestroMensaje("Cliente Agregado");
                dispose();
            }
        }
        if (e.getSource() == btnCancelar) { /** click btnCancelar **/
            dispose();
        }
    }
}
